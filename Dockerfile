FROM golang:1.8.5-jessie

RUN apt-get update && apt-get install -y graphviz
RUN go get -u github.com/TrueFurby/go-callvis
RUN cd $GOPATH/src/github.com/TrueFurby/go-callvis && make

COPY zmq.sh /tmp/zmq.sh
RUN /tmp/zmq.sh