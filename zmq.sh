#!/usr/bin/env bash

# Exiting on errors
set -e

# Set required version
VERSION="4.2.0"

echo "Downloading ZeroMQ"
wget https://github.com/zeromq/libzmq/releases/download/v${VERSION}/zeromq-${VERSION}.tar.gz

echo "Unpacking"
tar xvzf zeromq-${VERSION}.tar.gz

echo "Installing dependencies"
apt-get update && \
apt-get install -y libtool pkg-config build-essential autoconf automake uuid-dev

echo "Changing directory"
cd zeromq-${VERSION}

echo "Configuring"
./configure

echo "Compiling"
make

echo "Installing"
make install

echo "Installing ZeroMQ driver"
ldconfig

echo "Checking installation"
ldconfig -p | grep zmq
